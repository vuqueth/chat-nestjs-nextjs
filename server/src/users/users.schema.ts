import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsEmail } from 'class-validator';
import { HydratedDocument } from 'mongoose';

export type UserDocument = HydratedDocument<User>;

@Schema({
  timestamps: true,
})
export class User {
  @Prop()
  password: string;

  @Prop()
  first_name: string;

  @Prop()
  last_name: string;

  @Prop()
  middle_name: string;

  @Prop()
  phone: string;

  @Prop({ required: true, unique: true, index: true })
  @IsEmail()
  email: string;

  @Prop()
  is_active: boolean;

  @Prop()
  is_reported: boolean;

  @Prop()
  is_blocked: boolean;

  @Prop()
  preferences: string;

  @Prop()
  avatar_url: string;
}

export const UserFactory: ModelDefinition = {
  name: User.name,
  schema: SchemaFactory.createForClass(User).index({
    email: 'text',
    phone: 'text',
    first_name: 'text',
    last_name: 'text',
    middle_name: 'text',
  }),
};
