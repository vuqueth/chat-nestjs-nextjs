import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Message } from 'src/messages/messages.schema';
import { User } from 'src/users/users.schema';

@Schema({ timestamps: true })
export class DeletedMessage {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: Message.name,
  })
  messages_id: Message;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  users_id: User;
}

export const deletedMessageFactory: ModelDefinition = {
  name: DeletedMessage.name,
  schema: SchemaFactory.createForClass(DeletedMessage),
};
