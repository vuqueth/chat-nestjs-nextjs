import { Module } from '@nestjs/common';
import { DeletedMessagesService } from './deleted_messages.service';
import { DeletedMessagesController } from './deleted_messages.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { deletedMessageFactory } from './deleted_messages.schema';

@Module({
  imports: [MongooseModule.forFeature([deletedMessageFactory])],
  controllers: [DeletedMessagesController],
  providers: [DeletedMessagesService],
})
export class DeletedMessagesModule {}
