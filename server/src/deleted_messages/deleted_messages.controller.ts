import { Controller } from '@nestjs/common';
import { DeletedMessagesService } from './deleted_messages.service';

@Controller('deleted-messages')
export class DeletedMessagesController {
  constructor(
    private readonly deletedMessagesService: DeletedMessagesService,
  ) {}
}
