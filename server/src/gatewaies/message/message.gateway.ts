import { Logger, UseGuards } from '@nestjs/common';
import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { MessageWSGuard } from '../guards/message_ws.guard';
import { MessageGatewayService } from './message_gateway.service';
import { UsersService } from 'src/users/users.service';
import { InjectModel } from '@nestjs/mongoose';
import { Device } from 'src/devices/devices.schema';
import { Model } from 'mongoose';
import { Conversation } from 'src/conversation/conversation.schema';
import { Message } from 'src/messages/messages.schema';
import { Participant } from 'src/participants/participants.schema';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ObjectId = require('mongodb').ObjectId;
@UseGuards(MessageWSGuard)
@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class MessageGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer() server: Server;
  private logger: Logger = new Logger('MessageGateway');

  constructor(
    private messageGatewayService: MessageGatewayService,
    private usersService: UsersService,
    @InjectModel(Device.name)
    private deviceModel: Model<Device>,
    @InjectModel(Conversation.name)
    private conversationModel: Model<Conversation>,
    @InjectModel(Message.name)
    private messagesModel: Model<Message>,
    @InjectModel(Participant.name)
    private participantModel: Model<Participant>,
  ) {}

  afterInit(server: any) {
    this.logger.log(server, 'Init message gateway');
  }

  async handleConnection(client: Socket) {
    const accessToken = client.handshake.headers.authorization;
    try {
      const currentUser = await this.usersService.decodeAccessToken(
        accessToken,
      );

      if (!currentUser) {
        client.disconnect(true);
      }

      await this.deviceModel.create({
        users_id: currentUser._id,
        type: Device.TYPE.WEB,
        device_token: client.id,
      });
    } catch (err) {
      console.log('err connect ws: ', err);
      client.disconnect(true);
    }
  }

  async handleDisconnect(client: Socket) {
    this.logger.log(`${client.id} disconnected`);
    await this.deviceModel.deleteOne({
      device_token: client.id,
    });
  }

  @SubscribeMessage('send_message')
  async handleMessage(client: Socket, { currentUser, ...payload }: any) {
    /*
    Tạo conversation, message. Đã có conversation thì gắn vào message
    */
    const usersWatchReceiveMessage = [];
    let conversation: any = null;

    if (payload.fakeConversation) {
      const { users } = payload.fakeConversation;

      conversation = (
        await this.conversationModel.create({
          creator_id: currentUser._id,
        })
      ).toObject({ getters: true });
      await Promise.all(
        users.map((user) => {
          return this.participantModel.create({
            users_id: user._id,
            conversation_id: conversation._id,
          });
        }),
      );

      usersWatchReceiveMessage.push(...users);
    } else {
      conversation = await this.conversationModel.findById(
        new ObjectId(payload.conversation_id),
      );
      const participants = await this.participantModel.find(
        {
          conversation_id: new ObjectId(payload.conversation_id),
        },
        null,
        {
          populate: {
            path: 'users_id',
            select: {
              first_name: 1,
              middle_name: 1,
              last_name: 1,
              avatar_url: 1,
            },
          },
        },
      );
      usersWatchReceiveMessage.push(...participants.map((u) => u.users_id));
    }

    const message = (
      await this.messagesModel.create({
        conversation_id: conversation._id,
        sender_id: currentUser._id,
        message_type: payload.message_type,
        message: payload.message,
      })
    ).toObject({ getters: true });

    const devices = await this.deviceModel
      .find(
        {
          users_id: { $in: usersWatchReceiveMessage },
        },
        { device_token: 1 },
      )
      .lean();

    devices.forEach(({ device_token }) => {
      if (device_token) {
        this.server.to(device_token).emit('receive_message', {
          ...message,
          sender_id: currentUser,
          fakeId: payload.fakeId,
          fakeConversationId: payload?.fakeConversation?._id,
          conversation: {
            ...conversation,
            users: usersWatchReceiveMessage,
            messages: [{ ...message, sender_id: currentUser }],
          },
        });
      }
    });
  }
}
