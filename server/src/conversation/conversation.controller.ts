import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ConversationService } from './conversation.service';
import { InjectModel } from '@nestjs/mongoose';
import { Conversation } from './conversation.schema';
import { Model } from 'mongoose';
import { Message } from 'src/messages/messages.schema';
import { AccessTokenGuard } from 'src/auth/guards/accessToken.guard';
import { Participant } from 'src/participants/participants.schema';
import { CommonUtils } from 'utils/common';
import { omit } from 'lodash';
import { CreateGroup } from './conversation.dto';
import { User } from 'src/users/users.schema';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ObjectId = require('mongodb').ObjectId;
@Controller('conversations')
export class ConversationController {
  constructor(
    private readonly conversationService: ConversationService,
    @InjectModel(Conversation.name)
    private conversationModel: Model<Conversation>,
    @InjectModel(Message.name)
    private messageModel: Model<Message>,
    @InjectModel(Participant.name)
    private participantModel: Model<Participant>,
    @InjectModel(User.name)
    private userModel: Model<User>,
  ) {}

  @UseGuards(AccessTokenGuard)
  @Get()
  async find(@Request() request, @Query() query) {
    const userId = request.user.sub;
    const { limit = 15, skip = 0 } = query;

    const participantsOfCurrentUser = await this.participantModel
      .find(
        {
          users_id: new ObjectId(userId),
        },
        { conversation_id: 1 },
      )
      .lean();
    const conversationIds = participantsOfCurrentUser.map(
      (p) => p.conversation_id,
    );

    return this.conversationService.findConversationWithLastMessage(
      conversationIds,
      skip,
      limit,
    );
  }

  @UseGuards(AccessTokenGuard)
  @Put(':id')
  async update(@Param('id') id: string, @Body() requestUpdate) {
    try {
      await this.conversationModel.findOneAndUpdate({ _id: id }, requestUpdate);
    } catch (error) {
      console.log(error);
    }
  }

  @UseGuards(AccessTokenGuard)
  @Get(':id/messages')
  async findMessages(@Query() query, @Param('id') id: string) {
    const { limit = 30, skip = 0 } = query;
    console.log('query:::', query);

    if (!id) {
      return [];
    }

    const messages = await this.messageModel.find(
      { conversation_id: new ObjectId(id) },
      null,
      {
        sort: { message_id: -1 },
        skip,
        limit,
        populate: {
          path: 'sender_id',
          select: {
            first_name: 1,
            last_name: 1,
            middle_name: 1,
            avatar_url: 1,
            email: 1,
            phone: 1,
          },
        },
      },
    );

    return messages.sort((a, b) => a.message_id - b.message_id);
  }

  @UseGuards(AccessTokenGuard)
  @Get('search')
  async search(@Request() request, @Query() query) {
    const currentUserId = request.user.sub;
    const { filter: filterJson = '{}', skip = 0, limit = 10 } = query || {};

    const filter = CommonUtils.jsonValid(filterJson)
      ? JSON.parse(filterJson)
      : filterJson;
    const _q = filter?._q;
    const normalizeFilter = omit(filter, '_q');

    const participants = await this.participantModel.find(
      {
        users_id: new ObjectId(currentUserId),
      },
      { conversation_id: 1 },
    );

    const searchResult = await this.conversationModel
      .find(
        {
          ...normalizeFilter,
          _id: {
            $in: participants.map((p) => new ObjectId(p.conversation_id)),
          },
          $text: { $search: _q },
        },
        {
          score: { $meta: 'textScore' },
        },
        {
          skip,
          limit,
          sort: { score: { $meta: 'textScore' } },
        },
      )
      .lean();

    const conversationIds = searchResult.map((c) => c._id);

    return this.conversationService.findConversationWithLastMessage(
      conversationIds,
      skip,
      limit,
    );
  }

  @UseGuards(AccessTokenGuard)
  @Post('/group')
  async createGroup(@Body() requestBody: CreateGroup) {
    const conversation = (
      await this.conversationModel.create({
        title: requestBody.title,
        creator_id: requestBody.creator_id,
        avatar_url: requestBody.avatar_url,
      })
    ).toObject({ getters: true });

    requestBody.users.forEach(async (user_id) => {
      await this.participantModel.create({
        conversation_id: conversation._id,
        users_id: user_id,
      });
    });

    const users = await this.userModel.find(
      {
        _id: {
          $in: requestBody.users.map((userId) => new ObjectId(userId)),
        },
      },
      { first_name: 1, middle_name: 1, last_name: 1 },
    );

    // const message = (
    //   await this.messageModel.create({
    //     conversation_id: conversation._id,
    //     sender_id: requestBody.creator_id,
    //     message_type: Message.TYPE.TEXT,
    //     message: `${CommonUtils.getFullName(sender)} đã tạo cuộc hội thoại`,
    //   })
    // ).toObject({
    //   getters: true,
    // });

    return {
      ...conversation,
      users: users,
      // messages: [message],
    };
  }
}
