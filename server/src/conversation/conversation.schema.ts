import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { User } from 'src/users/users.schema';

@Schema({ timestamps: true, strict: false })
export class Conversation {
  @Prop()
  title: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  creator_id: User;

  @Prop({ require: true })
  channel_id: string;

  @Prop()
  background_url: string;

  @Prop()
  avatar_url: string;

  @Prop()
  key_search: string;
}

export const ConversationSchema = SchemaFactory.createForClass(
  Conversation,
).index({
  title: 'text',
  key_search: 'text',
});

export const conversationFactory: ModelDefinition = {
  name: Conversation.name,
  schema: ConversationSchema,
};
