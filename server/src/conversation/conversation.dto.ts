import { IsArray, IsString } from 'class-validator';

export class CreateGroup {
  @IsString()
  title: string;

  @IsString()
  avatar_url: string;

  @IsString()
  creator_id: string;

  @IsArray()
  users: string[];
}
