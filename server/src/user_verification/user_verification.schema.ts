import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { User } from 'src/users/users.schema';

@Schema({ timestamps: true })
export class UserVerification {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  users_id: User;

  @Prop()
  verification_code: string;
}

export const userVerificationFactory: ModelDefinition = {
  name: UserVerification.name,
  schema: SchemaFactory.createForClass(UserVerification),
};
