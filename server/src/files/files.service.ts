import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { File } from '../files/files.schema';

@Injectable()
export class FilesService {
  constructor(
    @InjectModel(File.name)
    private readonly filesModel: Model<File>,
  ) {}

  getFileUrl(file: Express.Multer.File) {
    return `http://localhost:3000/${file.filename}`;
  }

  async create(file: Express.Multer.File, user) {
    return this.filesModel.create({
      name: file.originalname,
      size: file.size,
      file_url: this.getFileUrl(file),
      create_by: user.sub,
    });
  }
}
