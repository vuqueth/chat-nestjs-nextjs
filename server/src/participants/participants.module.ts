import { Module } from '@nestjs/common';
import { ParticipantsService } from './participants.service';
import { ParticipantsController } from './participants.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { participantFactory } from './participants.schema';

@Module({
  controllers: [ParticipantsController],
  providers: [ParticipantsService],
  imports: [MongooseModule.forFeatureAsync([participantFactory])],
})
export class ParticipantsModule {}
