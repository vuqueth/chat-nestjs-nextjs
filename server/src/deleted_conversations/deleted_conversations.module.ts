import { Module } from '@nestjs/common';
import { DeletedConversationsService } from './deleted_conversations.service';
import { DeletedConversationsController } from './deleted_conversations.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { deletedConversationFactory } from './deleted_conversations.schema';

@Module({
  imports: [MongooseModule.forFeature([deletedConversationFactory])],
  controllers: [DeletedConversationsController],
  providers: [DeletedConversationsService],
})
export class DeletedConversationsModule {}
