import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { User } from 'src/users/users.schema';

export type DeviceDocument = HydratedDocument<Device>;

@Schema({ timestamps: true })
export class Device {
  @Prop({
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  users_id: User;

  // Id of client device
  @Prop()
  device_id: string;

  @Prop()
  type: string;

  @Prop()
  device_token: string;

  static TYPE = {
    WEB: 'web',
    APPLE: 'apple',
    ANDROID: 'android',
  };
}

export const deviceFactory: ModelDefinition = {
  name: Device.name,
  schema: SchemaFactory.createForClass(Device),
};
