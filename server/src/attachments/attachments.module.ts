import { Module } from '@nestjs/common';
import { AttachmentsService } from './attachments.service';
import { AttachmentsController } from './attachments.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { attachmentFactory } from './attachments.schema';

@Module({
  imports: [MongooseModule.forFeature([attachmentFactory])],
  controllers: [AttachmentsController],
  providers: [AttachmentsService],
})
export class AttachmentsModule {}
