import { Module } from '@nestjs/common';
import { ReportsService } from './reports.service';
import { ReportsController } from './reports.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { reportFactory } from './reports.schema';

@Module({
  controllers: [ReportsController],
  providers: [ReportsService],
  imports: [MongooseModule.forFeature([reportFactory])],
})
export class ReportsModule {}
