import {
  Row, Col, Form, Input,
} from 'antd';

export default function FullNameFormItem() {
  return (
    <Row gutter={[8, 8]}>
      <Col span={8}>
        <Form.Item
          name="first_name"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập họ',
            },
          ]}
        >
          <Input placeholder="* Họ" />
        </Form.Item>
      </Col>
      <Col span={8}>
        <Form.Item
          name="middle_name"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập tên đệm',
            },
          ]}
        >
          <Input placeholder="* Tên đệm" />
        </Form.Item>
      </Col>
      <Col span={8}>
        <Form.Item
          name="last_name"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập tên',
            },
          ]}
        >
          <Input placeholder="* Tên" />
        </Form.Item>
      </Col>
    </Row>
  );
}
