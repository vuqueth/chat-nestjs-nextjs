import { useDispatch, useSelector } from 'react-redux';

import { authActions, authSelector } from './authSlice';

import { User } from '@/models/userModel';

export function useCurrentUser() {
  return useSelector(authSelector.currentUser);
}

export function useSetCurrentUser() {
  const dispatch = useDispatch();
  return (user: User) => {
    dispatch(authActions.setCurrentUser(user));
  };
}
