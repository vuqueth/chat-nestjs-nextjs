/* eslint-disable camelcase */
import { useSelector } from 'react-redux';
import { Empty } from 'antd';

import Conversation from '../conversation/Conversation';
import { chatSelector } from '../chatSlice';

import { commonSelector } from '@/features/common/commonSlice';
import { useCurrentUser } from '@/features/auth/authHooks';
import { Conversation as ConversationDocument } from '@/models/conversationModel';
import CommonUtil from '@/utils/common.util';

export default function ListConversation() {
  const searchingConversation = useSelector(commonSelector.searchingConversation);
  const searchingResultConversation = useSelector(commonSelector.searchingResultConversation);
  const currentUser = useCurrentUser();
  const listConversation = useSelector(chatSelector.listConversation);

  const renderConversation = (list: ConversationDocument[]) => (
    <>
      {list.map(({
        users, messages, _id, avatar_url, title,
      }: ConversationDocument) => {
        const participant = users.find((e) => e._id !== currentUser?._id);
        const avatarUrl = avatar_url || participant?.avatar_url;
        const lastMessage = messages?.[messages.length - 1] || 0;

        if (!_id) {
          return null;
        }

        return (
          <Conversation
            key={_id}
            name={title || CommonUtil.getFullName(participant) || ''}
            avatarUrl={avatarUrl}
            lastMessage={lastMessage}
            _id={_id}
          />
        );
      })}
    </>
  );

  if (!searchingConversation) {
    return renderConversation(listConversation);
  }

  if (searchingConversation && !searchingResultConversation.length) {
    return <Empty />;
  }

  return renderConversation(searchingResultConversation);
}
