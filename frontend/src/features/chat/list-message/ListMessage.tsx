/* eslint-disable react/no-array-index-key */
import React, {
  useCallback, useEffect, useMemo, useRef,
} from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useDispatch } from 'react-redux';

import MessageItem, { MessageItemProps } from '../message/Message';
import { useActiveConversation } from '../chatHooks';
import { chatActions } from '../chatSlice';

import styles from './Styles.module.scss';

interface ListMessageProps {
  dataSource?: MessageItemProps[];
}

const ListMessage: React.FC<ListMessageProps> = React.memo((props) => {
  const { dataSource = [] } = props;
  const scrollRef = useRef<HTMLDivElement>(null);
  const activeConversation = useActiveConversation();
  const dispatch = useDispatch();
  const skip = useMemo(() => activeConversation?.skip || 0, [activeConversation]);

  const loadMoreMessage = useCallback(() => {
    if (!activeConversation?._id) return;

    dispatch(chatActions.requestListMessage({ conversation_id: activeConversation._id, skip: skip + 30 }));
    const scrollHeight = scrollRef.current?.scrollHeight;

    if (!scrollHeight) return;

    scrollRef.current?.scrollTo({
      top: -(scrollHeight - 1000),
    });
  }, [skip]);

  useEffect(() => {
    (window as any).scrollRef = scrollRef;
  }, [scrollRef]);

  return (
    <div
      ref={scrollRef}
      id="scrollableDiv"
      className={styles['chat-list']}
    >
      <InfiniteScroll
        dataLength={dataSource.length}
        next={loadMoreMessage}
        inverse
        hasMore={dataSource.length % 10 === 0}
        loader={null}
        scrollableTarget="scrollableDiv"
      >
        {dataSource.map((messageItemProps: MessageItemProps, index: number) => <MessageItem key={`${messageItemProps.id}_${index}`} {...messageItemProps} />)}
      </InfiniteScroll>
    </div>
  );
});
export default ListMessage;
