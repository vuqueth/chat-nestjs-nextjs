import { useSelector } from 'react-redux';

import { chatSelector } from './chatSlice';

export function useActiveConversation() {
  return useSelector(chatSelector.listConversation).find((conversation) => conversation.active);
}
