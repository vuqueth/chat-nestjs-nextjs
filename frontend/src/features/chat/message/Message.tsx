/* eslint-disable camelcase */
import classNames from 'classnames';
import { Avatar, Typography } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import moment from 'moment';
import React, { useEffect, useState } from 'react';

import styles from './Styles.module.scss';
import LikeChat from './LikeChat';
import RemoveChat from './RemoveChat';
import ReplyChat from './ReplyChat';

export interface MessageItemProps {
  id?: string;
  message_type?: 'text' | 'image' | 'other';
  position?: 'left' | 'right';
  removeAction?: boolean;
  replyAction?: boolean;
  likeAction?: boolean;
  avatarUrl?: string;
  time?: string;
  sending: boolean;
  message?: any;
  showAvatar?: boolean;
}

const MessageItem: React.FC<MessageItemProps> = React.memo(({
  message_type = 'text',
  position,
  removeAction = true,
  replyAction = true,
  likeAction = true,
  avatarUrl,
  time,
  sending,
  showAvatar,
  message,
}) => {
  const [mouseEnter, setMouseEnter] = useState<boolean>(false);
  const isLeft = position === 'left';
  const isRight = position === 'right';

  const renderTime = (t: string) => {
    if (!t) return null;
    const timeMoment = moment(t);
    const isToday = timeMoment.day() === moment().day();
    if (isToday) {
      return timeMoment.format('hh:mm');
    }

    const isYear = timeMoment.year() === moment().year();
    if (isYear) {
      return timeMoment.format('hh:mm DD/MM');
    }

    return timeMoment.format('hh:mm DD/MM/YYYY');
  };

  useEffect(() => {
    setMouseEnter(false);
  }, []);

  return (
    <div
      className={classNames(
        styles['chat-item'],
        {
          [styles['position-left']]: isLeft,
          [styles['position-right']]: isRight,
        },
      )}
      onMouseEnter={() => setMouseEnter(true)}
      onMouseLeave={() => setMouseEnter(false)}
    >
      <div className={classNames(styles.message, {
        [styles['message-left']]: isLeft,
      })}
      >
        <div className={classNames(styles.action, {
          [styles['action-with-message-right']]: isRight,
        })}
        >
          {mouseEnter && (
            <>
              {removeAction && <RemoveChat />}
              {replyAction && <ReplyChat />}
            </>
          )}
        </div>
        <div className={styles.text}>
          {message}
          <div className={styles['footer-message']}>
            {mouseEnter && (
            <Typography.Paragraph className={styles.time} type="secondary">
              {renderTime(time || '')}
            </Typography.Paragraph>
            )}
            {sending && (
            <Typography.Paragraph className={styles.time} type="secondary">
              Đang gửi
            </Typography.Paragraph>
            )}
            {mouseEnter && !sending && (
            <Typography.Paragraph className={styles.time} type="secondary">
              Đã gửi
            </Typography.Paragraph>
            )}
          </div>
          {isLeft && mouseEnter && likeAction && <LikeChat />}
        </div>
        {showAvatar ? (
          <Avatar
            size="large"
            icon={<UserOutlined />}
            src={avatarUrl}
            className={styles.avatar}
          />
        ) : <div className={styles.avatarBox} />}
      </div>
    </div>
  );
});
export default MessageItem;
