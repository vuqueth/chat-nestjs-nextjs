import React from 'react';
import { DeleteOutlined } from '@ant-design/icons';
import { Button } from 'antd';

export default function RemoveChat(props: Omit<React.HTMLAttributes<HTMLDivElement>, 'ref'>) {
  return (
    <div {...props}>
      <Button style={{ backgroundColor: '#ededed70' }} type="text" icon={<DeleteOutlined />} />
    </div>
  );
}
