/* eslint-disable camelcase */
import {
  Avatar, Button, Drawer, Typography,
} from 'antd';
import moment from 'moment';
import {
  InfoCircleOutlined,
  PhoneOutlined, UserOutlined, VideoCameraOutlined,
} from '@ant-design/icons';
import { useMemo, useState } from 'react';

import SettingConversation from '../setting-conversation/SettingConversation';
import { useActiveConversation } from '../chatHooks';

import styles from './Styles.module.scss';

import { useCurrentUser } from '@/features/auth/authHooks';
import CommonUtil from '@/utils/common.util';

export default function ChatOnboardHeader() {
  const [showSetting, setShowSetting] = useState<boolean>(false);
  const activeConversation = useActiveConversation();
  const currentUser = useCurrentUser();

  const participant = useMemo(() => activeConversation?.users.find((user) => user._id !== currentUser?._id), [activeConversation]);
  const { title, avatar_url } = activeConversation || {};

  return (
    <div className={styles['header-content']}>
      <div className={styles.receiver}>
        <Avatar src={avatar_url || participant?.avatar_url} className={styles.avatar} size="large" icon={<UserOutlined />} />
        <div className={styles['preview-conversation']}>
          <Typography.Title level={5} className={styles.name}>{title || CommonUtil.getFullName(participant)}</Typography.Title>
          {!title && !avatar_url && (
          <Typography.Paragraph
            className={styles.message}
            type="secondary"
          >
            {`Truy cập ${moment().format('DD/MM')}`}
          </Typography.Paragraph>
          )}
        </div>
      </div>
      <div className={styles.action}>
        <Button type="text">
          <PhoneOutlined style={{ fontSize: 20 }} />
        </Button>
        <Button type="text">
          <VideoCameraOutlined style={{ fontSize: 20 }} />
        </Button>
        <Button type="text" onClick={() => setShowSetting(true)}>
          <InfoCircleOutlined style={{ fontSize: 20 }} />
        </Button>
      </div>
      <Drawer
        title="Thông tin hội thoại"
        onClose={() => setShowSetting(false)}
        open={showSetting}
      >
        <SettingConversation setShowSetting={setShowSetting} />
      </Drawer>
    </div>
  );
}
