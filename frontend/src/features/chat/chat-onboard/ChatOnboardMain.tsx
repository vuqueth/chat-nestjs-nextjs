/* eslint-disable camelcase */
import React, { useMemo } from 'react';

import { useActiveConversation } from '../chatHooks';
import InputChat from '../input-chat/InputChat';
import ListMessage from '../list-message/ListMessage';

import styles from './Styles.module.scss';

import { useCurrentUser } from '@/features/auth/authHooks';
import { IMessage } from '@/models/messagesModel';

export default function ChatOnboardMain() {
  const currentUser = useCurrentUser();
  const activeConversation = useActiveConversation();
  const messages = activeConversation?.messages || [];

  const renderBackground = useMemo(() => {
    if (activeConversation?.background_url) {
      return {
        backgroundImage: `url(${activeConversation?.background_url})`,
      };
    }

    return {
      backgroundColor: 'hsla(0, 2%, 90%, 0.43)',
    };
  }, [activeConversation]);

  const dataSource = messages.map(({
    message,
    sender_id,
    _id,
    createdAt,
    message_type,
    sending,
  }: IMessage, index: number) => {
    const currentUserIsSender = sender_id?._id === currentUser?._id;
    const currentPosition: 'left' | 'right' = currentUserIsSender ? 'right' : 'left';
    const showAvatar = ((messages?.[index - 1]?.sender_id?._id === currentUser?._id) ? 'right' : 'left') !== currentPosition;

    return {
      id: _id,
      position: currentPosition,
      time: createdAt,
      status: 'read',
      avatarUrl: sender_id.avatar_url,
      message,
      showAvatar,
      sending,
      message_type,
    };
  });

  return (
    <div style={renderBackground} className={styles['main-content']}>
      <ListMessage dataSource={dataSource} />
      <InputChat />
    </div>
  );
}
