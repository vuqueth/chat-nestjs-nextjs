import { Button, Form, Modal } from 'antd';
import { BgColorsOutlined } from '@ant-design/icons';
import { useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';

import { chatActions } from '../chatSlice';
import { useActiveConversation } from '../chatHooks';

import UploadImage from '@/components/upload/UploadImage';
import ConversationApi from '@/api/ConversationApi';

export default function SettingConversation({ setShowSetting }: any) {
  const [showChangeBackground, setShowChangeBackground] = useState(false);
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const activeConversation = useActiveConversation();
  const oldBackground = useMemo(() => activeConversation?.background_url, []);

  const handleChangeBackground = () => {
    const backgroundUrl = form.getFieldValue('background_url');
    if (backgroundUrl && activeConversation) {
      dispatch(chatActions.updateConversation({
        ...activeConversation,
        background_url: backgroundUrl,
      }));
      ConversationApi.updateConversation(activeConversation._id, { background_url: backgroundUrl });
    }
    setShowChangeBackground(false);
  };

  const handleCancelChangeBackground = () => {
    setShowChangeBackground(false);

    if (activeConversation) {
      dispatch(chatActions.updateConversation({
        ...activeConversation,
        background_url: oldBackground,
      }));
    }
  };

  return (
    <div>
      <Button
        onClick={() => {
          setShowSetting(false);
          setShowChangeBackground(true);
        }}
        type="text"
        icon={<BgColorsOutlined />}
      >
        Hình nền
      </Button>
      <Modal
        open={showChangeBackground}
        title="Thay đổi hình nền"
        destroyOnClose
        onCancel={handleCancelChangeBackground}
        onOk={handleChangeBackground}
        cancelText="Hủy"
        okText="Cập nhật"
      >
        <Form form={form}>
          <Form.Item name="background_url">
            <UploadImage
              onChange={(e) => {
                if (e && activeConversation) {
                  dispatch(chatActions.updateConversation({
                    ...activeConversation,
                    background_url: e,
                  }));
                }
              }}
              listType="picture-card"
            />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
