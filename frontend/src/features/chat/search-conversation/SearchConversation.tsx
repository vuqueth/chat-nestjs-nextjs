import { SearchOutlined } from '@ant-design/icons';
import { Button, Input } from 'antd';
import { debounce } from 'lodash';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';

import styles from './Styles.module.scss';

import { commonActions } from '@/features/common/commonSlice';
import ConversationApi from '@/api/ConversationApi';

export function CloseSearchConversation() {
  const dispatch = useDispatch();

  return (
    <Button type="text" onClick={() => dispatch(commonActions.setSearchingConversation(false))}>
      Đóng
    </Button>
  );
}

export default function SearchConversation() {
  const dispatch = useDispatch();

  const onSearch = debounce(async (e) => {
    const response = await ConversationApi.searchConversation(e.target.value);

    if (Array.isArray(response)) {
      dispatch(commonActions.setSearchingResultConversation(response));
    }
  }, 300);

  const onFocus = () => {
    dispatch(commonActions.setSearchingConversation(true));
  };

  useEffect(() => () => {
    dispatch(commonActions.setSearchingConversation(false));
  }, []);

  return (
    <div className={styles['search-conversation']}>
      <Input
        placeholder="Tìm kiếm"
        onFocus={onFocus}
        onChange={onSearch}
        prefix={<SearchOutlined />}
      />
    </div>
  );
}
