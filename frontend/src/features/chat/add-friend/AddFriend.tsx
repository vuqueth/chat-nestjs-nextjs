import { SearchOutlined, UserAddOutlined } from '@ant-design/icons';
import { useEffect, useRef, useState } from 'react';
import {
  Button, Empty, Input, Modal,
} from 'antd';
import { debounce } from 'lodash';
import { useDispatch, useSelector } from 'react-redux';

import { chatActions, chatSelector } from '../chatSlice';

import styles from './Styles.module.scss';
import ResultItemSearchUser from './SearchUserResult';

import UserApi from '@/api/UserApi';
import { User } from '@/models/userModel';

export default function AddFriend() {
  const dispatch = useDispatch();
  const showModal = useSelector(chatSelector.showAddFriendModal);
  const inputRef = useRef<any>(null);

  const [users, setUser] = useState<User[]>([]);

  const showingModal = () => dispatch(chatActions.setShowAddFriendModal(true));

  const closeModal = () => dispatch(chatActions.setShowAddFriendModal(false));

  const onSearch = debounce(async (e) => {
    const listUser: any = await UserApi.searchUser(e.target.value);
    setUser(listUser);
  }, 300);

  useEffect(() => inputRef?.current?.focus(), [showModal]);

  return (
    <div>
      <Button type="text" onClick={showingModal}>
        <UserAddOutlined />
      </Button>
      <Modal
        open={showModal}
        destroyOnClose
        onCancel={closeModal}
        title="Thêm bạn"
        footer={null}
      >
        <Input
          ref={inputRef}
          placeholder="Tìm kiếm"
          onChange={onSearch}
          prefix={<SearchOutlined />}
          allowClear
        />
        <div className={styles.result}>
          {users.length ? users.map((user: any) => (
            <ResultItemSearchUser
              key={user._id}
              user={user}
            />
          )) : <Empty />}
        </div>
      </Modal>
    </div>
  );
}
