import { PayloadAction } from '@reduxjs/toolkit';
import { put, select } from 'redux-saga/effects';
import { isEqual } from 'lodash';

import { chatActions, chatSelector } from './chatSlice';

import { CreateConversation } from '@/models/authModel';
import { Conversation } from '@/models/conversationModel';

export default function* createConversation({ payload }: PayloadAction<CreateConversation>) {
  const listConversation: Conversation[] = yield select(chatSelector.listConversation);
  const activeConversation = listConversation.find((conversation: Conversation) => isEqual([payload.currentUser._id, payload.user._id].sort(), conversation.users.map((e) => e._id).sort()));

  if (activeConversation?._id) {
    yield put(chatActions.activeConversation({ conversationId: activeConversation._id, active: true }));
    yield put(chatActions.requestListMessage({ conversation_id: activeConversation._id }));
  }
}
