import { io } from 'socket.io-client';

import { AuthUtils } from '@/utils/auth.util';

export function connectSocket() {
  const socket = io(process.env.NEXT_PUBLIC_API_DOMAIN || '', {
    extraHeaders: {
      Authorization: AuthUtils.getAccessToken() || '',
    },
  });

  return new Promise((resolve) => {
    socket.on('connect', () => {
      resolve(socket);
    });
  });
}
