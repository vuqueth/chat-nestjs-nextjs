/* eslint-disable @typescript-eslint/no-empty-function */
import { PayloadAction, createSlice, current } from '@reduxjs/toolkit';
import {
  isEqual, omit, sortBy,
} from 'lodash';
import { uuid } from 'uuidv4';

import { RootState } from '@/app/store';
import { IMessage } from '@/models/messagesModel';
import { Conversation, IActiveConversation, IRequestMessagesByConversation } from '@/models/conversationModel';
import { CreateConversation } from '@/models/authModel';

interface ChatState {
  socketConnected: boolean;
  showAddFriendModal: boolean;

  loading: boolean;
  error: string;
  listConversation: Conversation[];
  loaded: boolean;
}

const initialState: ChatState = {
  socketConnected: false,
  showAddFriendModal: false,
  loading: false,
  loaded: false,
  error: '',
  listConversation: [],
};

const chatSlice = createSlice({
  name: 'chat',
  initialState,
  reducers: {
    setSocketConnected(state, { payload }: PayloadAction<boolean>) {
      state.socketConnected = payload;
    },

    setShowAddFriendModal(state, { payload }: PayloadAction<boolean>) {
      state.showAddFriendModal = payload;
    },

    sendMessage(state, { payload }: PayloadAction<IMessage>) {
      /*
        Tạo fake message, sending = true cho active conversation
      */

      const sortedItems = sortBy(state.listConversation, ({ _id }) => (_id === payload.conversation_id ? 0 : 1));
      state.listConversation = sortedItems.map((c) => {
        if (c._id === payload.conversation_id) {
          return {
            ...c,
            messages: [...c.messages, payload],
          };
        }

        return c;
      });
    },

    sendMessageSuccess(state, { payload }: PayloadAction<IMessage>) {
      const { fakeConversationId } = payload;
      let currentUserIsSender = false;
      let currentUserHasConversation = false;

      const listConversationUpdated = state.listConversation.map((conversation) => {
        if (conversation._id === fakeConversationId || conversation._id === payload.conversation_id) {
          currentUserHasConversation = true;
          const sendedMessages = conversation.messages.map((message) => {
            if (message._id === payload.fakeId) {
              currentUserIsSender = true;
              return omit(payload, ['fakeConversationId', 'fakeId']);
            }

            return current(message);
          });

          if (!currentUserIsSender) {
            /*
              Không phải người gửi thì không có fake message, vậy cần cờ `currentUserIsSender` để thêm message mới.
            */
            sendedMessages.push(omit(payload, ['fakeConversationId', 'fakeId']));
          }

          return {
            ...conversation,
            messages: sendedMessages,
            _id: payload.conversation_id,
            fakeId: undefined,
          };
        }

        return conversation;
      });

      if (!currentUserIsSender && !currentUserHasConversation && payload.conversation) {
        listConversationUpdated.push({
          ...payload.conversation,
          error: '',
          loaded: true,
          active: false,
        });
      }

      state.listConversation = sortBy(listConversationUpdated, ({ _id }) => (_id === payload.conversation_id ? 0 : 1));
    },

    createConversation(state, { payload }: PayloadAction<CreateConversation>) {
      /*
      Tìm conversation của hai currentUser và bạn.
      => Y: active conversation => requestMessage
        N: Tạo conversation fake, sau đó sendMessage >> sendMessageSuccess thì update lại.
      */
      const { currentUser, user } = payload;
      const conversationByUsers = state.listConversation.find((conversation: Conversation) => isEqual([currentUser._id, user._id].sort(), conversation.users.map((e) => e._id).sort()));
      if (!conversationByUsers) {
        const fakeId = uuid();
        const fakeConversation: any = {
          users: [currentUser, user],
          messages: [],
          _id: fakeId,
          fakeId,
          active: true,
          error: '',
          loaded: false,
        };
        state.listConversation.push(fakeConversation);
      }
    },

    activeConversation(state, { payload }: PayloadAction<IActiveConversation>) {
      const { active, conversationId } = payload;
      state.listConversation = state.listConversation.map((conversation) => {
        if (conversation._id === conversationId) {
          return { ...conversation, active };
        }

        return { ...conversation, active: false };
      });
    },

    updateConversation(state, { payload }: PayloadAction<Conversation>) {
      const { _id } = payload;
      state.listConversation = state.listConversation.map((conversation) => {
        if (conversation._id === _id) {
          return { ...conversation, ...payload };
        }

        return conversation;
      });
    },

    requestListConversation(state) {
      state.loading = true;
    },

    requestListConversationSuccess(state, { payload }: PayloadAction<Conversation[]>) {
      state.loading = false;
      state.listConversation = [
        ...state.listConversation,
        ...payload,
      ];
      state.loaded = true;
    },

    requestListConversationFail(state, { payload }: PayloadAction<string>) {
      state.loaded = false;
      state.loading = false;
      state.error = payload;
    },

    requestListMessage(state, { payload }: PayloadAction<IRequestMessagesByConversation>) {
      if (payload.conversation_id) {
        state.listConversation = state.listConversation.map((c) => {
          if (payload.conversation_id === c._id) {
            return {
              ...c, error: '', loaded: false,
            };
          }

          return c;
        });
      }
    },

    requestListMessageSuccess(state, { payload }: PayloadAction<IRequestMessagesByConversation>) {
      if (payload.conversation_id) {
        state.listConversation = state.listConversation.map((c) => {
          if (payload.conversation_id === c._id) {
            return {
              ...c,
              messages: payload.messages || [],
              error: '',
              loaded: true,
              skip: payload?.skip || 0,
            };
          }

          return c;
        });
      }
    },

    requestListMessageFail(state, { payload }: PayloadAction<IRequestMessagesByConversation>) {
      if (payload.conversation_id) {
        state.listConversation = state.listConversation.map((c) => {
          if (payload.conversation_id === c._id) {
            return { ...c, error: payload.error || '', loaded: false };
          }

          return c;
        });
      }
    },

    createGroupConversation(state, { payload }: PayloadAction<any>) {},
    createGroupConversationSuccess(state, { payload }: PayloadAction<any>) {
      state.listConversation.push(payload);
    },
  },
});

const chatReducer = chatSlice.reducer;
export default chatReducer;

export const chatStoreName = chatSlice.name;

export const chatActions = chatSlice.actions;

export const chatSelector = {
  socketConnected: (state: RootState) => state.chat.socketConnected,
  showAddFriendModal: (state: RootState) => state.chat.showAddFriendModal,
  loading: (state: RootState) => state.chat.loading,
  loaded: (state: RootState) => state.chat.loaded,
  error: (state: RootState) => state.chat.error,
  listConversation: (state: RootState) => state.chat.listConversation,
};
