import Cookies from 'js-cookie';

import { JwtPayload } from '@/models/authModel';
import { AUTH_KEY } from '@/constants/auth';

export const AuthUtils = {
  getAuthToken: () => ({
    accessToken: Cookies.get(AUTH_KEY.ACCESS_TOKEN),
    refreshToken: Cookies.get(AUTH_KEY.REFRESH_TOKEN),
  }),
  getAccessToken(): string | undefined {
    return Cookies.get(AUTH_KEY.ACCESS_TOKEN);
  },
  getExpiresAtAccessToken(): string | undefined {
    return Cookies.get(AUTH_KEY.EXPIRES_AT);
  },
  getRefreshToken(): string | undefined {
    return Cookies.get(AUTH_KEY.REFRESH_TOKEN);
  },
  setAuthToken: ({
    accessToken, refreshToken, expiresIn, expiresAt,
  }: JwtPayload) => {
    if (accessToken && expiresIn) {
      const expireTime = new Date(new Date().getTime() + 1000 * +expiresIn);

      Cookies.set(AUTH_KEY.EXPIRES_IN, expiresIn, {
        expires: expireTime,
      });
      Cookies.set(AUTH_KEY.ACCESS_TOKEN, accessToken, {
        expires: expireTime,
      });
      Cookies.set(AUTH_KEY.EXPIRES_AT, expiresAt, {
        expires: expireTime,
      });
    }

    if (refreshToken) {
      Cookies.set(AUTH_KEY.REFRESH_TOKEN, refreshToken);
    }
  },
  refreshAuthPayload: async () => {
    const refreshToken = Cookies.get(AUTH_KEY.REFRESH_TOKEN);
    if (refreshToken) {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_DOMAIN}/auth/refresh`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${refreshToken}`,
        },
      });
      return response.json();
    }
    return null;
  },
  removeAllToken() {
    Cookies.remove(AUTH_KEY.ACCESS_TOKEN);
    Cookies.remove(AUTH_KEY.EXPIRES_IN);
    Cookies.remove(AUTH_KEY.EXPIRES_AT);
    Cookies.remove(AUTH_KEY.REFRESH_TOKEN);
  },
};
