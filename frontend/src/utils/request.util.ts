import axios from 'axios';
import Cookies from 'js-cookie';
import { notification } from 'antd';

import { AuthUtils } from './auth.util';

import { AUTH_KEY } from '@/constants/auth';
import { JwtPayload } from '@/models/authModel';

const RequestUtil = {
  axios: axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_DOMAIN,
    timeout: 5 * 60 * 1000,
    headers: {
      'Content-Type': 'application/json',
    },
    withCredentials: false,
  }),
};
export default RequestUtil;

// Make sure the request have latest access token
RequestUtil.axios.interceptors.request.use((config: any = {}) => ({
  ...config,
  headers: {
    ...config.header,
    Authorization: `Bearer ${Cookies.get(AUTH_KEY.ACCESS_TOKEN)}`,
  },
}), (error) => Promise.reject(error));

// Refresh accessToken when it expires
RequestUtil.axios.interceptors.response.use(
  (response) => response?.data,
  async (error) => {
    const originalRequest = error.config;

    if (error?.response?.status === 401 && !originalRequest?.retry) {
      originalRequest.retry = true;

      try {
        const authPayload: JwtPayload = await AuthUtils.refreshAuthPayload();

        AuthUtils.setAuthToken(authPayload);

        RequestUtil.axios.defaults.headers.Authorization = `Bearer ${authPayload.accessToken}`;
        originalRequest.headers.Authorization = `Bearer ${authPayload.accessToken}`;

        return RequestUtil.axios(originalRequest);
      } catch (err: any) {
        window.location.href = '/login';
        notification.error({ message: '', description: 'Đã hết phiên sử dụng, vui lòng đăng nhập lại' });
      }
    }

    return Promise.reject(error);
  },
);
