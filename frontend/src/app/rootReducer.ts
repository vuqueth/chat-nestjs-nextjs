import { combineReducers } from '@reduxjs/toolkit';
import { routerReducer } from 'connected-next-router';

import authReducer, { authStoreName } from '@/features/auth/authSlice';
import chatReducer, { chatStoreName } from '@/features/chat/chatSlice';
import commonReducers, { commonStoreName } from '@/features/common/commonSlice';

const rootReducer = combineReducers({
  router: routerReducer,
  [commonStoreName]: commonReducers,
  [authStoreName]: authReducer,
  [chatStoreName]: chatReducer,
});
export default rootReducer;
