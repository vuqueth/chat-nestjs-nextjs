import { configureStore } from '@reduxjs/toolkit';
import { createRouterMiddleware } from 'connected-next-router';
import { createWrapper } from 'next-redux-wrapper';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

const sagaMiddleware = createSagaMiddleware();
const routerMiddleware = createRouterMiddleware();

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => [...getDefaultMiddleware(), sagaMiddleware, routerMiddleware],
  devTools: true,
});

sagaMiddleware.run(rootSaga);

const makeStore = () => store;

export type AppStore = ReturnType<typeof makeStore>;
export type RootState = ReturnType<typeof store.getState>;

export const wrapper = createWrapper<AppStore>(makeStore);
