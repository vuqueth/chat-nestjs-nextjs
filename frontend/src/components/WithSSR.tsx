import { useEffect, useState } from 'react';

export default function WithNoSsr({ children }: {children: any}) {
  const [isSsr, setIsSsr] = useState(false);

  useEffect(() => {
    setIsSsr(true);
  }, []);

  if (!isSsr) return null;

  return children;
}
