import { Button, Modal, ModalProps } from 'antd';
import { useRef } from 'react';

import { FormModalContext } from './Context';

export default function FormModal({ children, ...modalProps }: ModalProps) {
  const submitRef: any = useRef(null);

  return (
    // eslint-disable-next-line react/jsx-no-constructed-context-values
    <FormModalContext.Provider value={{ submitRef }}>
      <Modal
        {...modalProps}
        footer={(
          <div>
            <Button onClick={(e: any) => {
              modalProps?.onCancel?.(e);
            }}
            >
              {modalProps.cancelText || 'Hủy'}
            </Button>
            <Button
              type="primary"
              onClick={(e: any) => {
                modalProps?.onOk?.(e);
                submitRef?.current?.click();
              }}
            >
              {modalProps.okText || 'Cập nhật'}
            </Button>
          </div>
        )}
      >
        {children}
      </Modal>
    </FormModalContext.Provider>
  );
}
