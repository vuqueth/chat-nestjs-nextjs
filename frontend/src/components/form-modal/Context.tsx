import { createContext } from 'react';

export const FormModalContext = createContext({
  submitRef: null,
});
