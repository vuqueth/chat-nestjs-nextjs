import type { AppProps } from 'next/app';
import { Provider, useDispatch } from 'react-redux';
import React, { useCallback, useEffect, useRef } from 'react';

import '@/styles/globals.css';
import { store, wrapper } from '@/app/store';
import { AuthUtils } from '@/utils/auth.util';
import { authActions } from '@/features/auth/authSlice';
import { JwtPayload } from '@/models/authModel';

function App({ Component, pageProps }: AppProps) {
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (!AuthUtils.getRefreshToken()) return;

    dispatch(authActions.fetchCurrentUser());
  }, []);

  const intervalRef = useRef<any>(null);

  const getToken = useCallback(
    async () => {
      const expiresAt = AuthUtils.getExpiresAtAccessToken();
      if (expiresAt && (new Date() > new Date(+expiresAt - 5 * 60 * 1000)) && AuthUtils.getRefreshToken()) {
        const authPayload: JwtPayload = await AuthUtils.refreshAuthPayload();
        AuthUtils.setAuthToken(authPayload);
      }
    },
    [],
  );

  useEffect(() => {
    const interval = setInterval(() => getToken(), 1000);
    intervalRef.current = interval;
    return () => clearInterval(interval);
  }, [getToken]);

  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default wrapper.withRedux(App);
