import {
  Button, Col, Form, Input, Row, Typography,
} from 'antd';
import React from 'react';
import { useDispatch } from 'react-redux';
import Link from 'next/link';

import styles from '@/styles/Start.module.scss';
import { authActions } from '@/features/auth/authSlice';
import { SignupPayload } from '@/models/authModel';
import FullNameFormItem from '@/features/user-settings/components/information/FullNameFormItem';

export default function Start() {
  const dispatch = useDispatch();

  const signup = (formValue: SignupPayload) => {
    dispatch(authActions.signup(formValue));
  };

  return (
    <div className={styles.start}>
      <div className={styles.title}>
        <Typography.Title level={1}>
          What
          {'\''}
          s your name ?
        </Typography.Title>
        <Typography.Paragraph>Vui lòng nhập thông tin cá nhân để bắt đầu sử dụng</Typography.Paragraph>
      </div>
      <Form
        onFinish={signup}
        autoComplete="off"
        style={{ maxWidth: 500 }}
      >
        <FullNameFormItem />
        <Form.Item
          name="email"
          rules={[{ required: true, message: 'Vui lòng nhập chính xác Email', type: 'email' }]}
        >
          <Input
            autoFocus
            className={styles.input}
            placeholder="* Email"
          />
        </Form.Item>
        <Row gutter={[8, 8]}>
          <Col span={12}>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập mật khẩu',
                },
                {
                  min: 6,
                  message: 'Mật khẩu phải có ít nhất 6 kí tự',
                },
              ]}
            >
              <Input.Password placeholder="* Mật khẩu" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="repeat_password"
              dependencies={['password']}
              rules={[
                {
                  required: true,
                  message: 'Vui lòng xác nhận mật khẩu!',
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error('Mật khẩu xác nhận không chính xác'));
                  },
                }),
              ]}
            >
              <Input.Password placeholder="* Xác nhận mật khẩu" />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item>
          <div className={styles.wrapperButton}>
            <Button htmlType="submit" className={styles.button} type="primary" size="large">Tiếp tục</Button>
          </div>
        </Form.Item>
        <div className={styles.login}>
          <Link href="/login">
            Đăng nhập tại đây -
            {'>'}
          </Link>
        </div>
      </Form>
    </div>
  );
}
