import { User } from './userModel';

export interface JwtPayload {
  accessToken: string;
  refreshToken: string;
  expiresIn: string;
  expiresAt: string;
  currentUser?: User;
}

export interface LoginPayload {
  email: string;
  password: string;
}

export interface SignupPayload {
  email: string;
  first_name: string;
  middle_name: string;
  last_name: string;
  password: string;
  repeat_password: string;
}

export interface ResponseFail {
  error: string;
  message: string;
  statusCode: number;
}

export interface CreateConversation {
  currentUser: User;
  user: User;
}
